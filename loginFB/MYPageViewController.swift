//
//  MYPageViewController.swift
//  loginFB
//
//  Created by juan harold kuga palomino on 29/11/16.
//  Copyright © 2016 juan harold kuga palomino. All rights reserved.
//

import UIKit

class MYPageViewController: UIPageViewController {
    
    lazy var arrayViewController : [UIViewController] = {
        
        let red = self.giveViewController(color: "Red")
        let green = self.giveViewController(color: "Green")
        let yellow = self.giveViewController(color: "Yellow")
        
        return [red, green, yellow]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.setViewControllers([arrayViewController.first!], direction: .forward, animated: true, completion: nil)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func giveViewController(color : String)->UIViewController{
        let storyBoardID = "\(color)VC"
        let viewContoller = self.storyboard?.instantiateViewController(withIdentifier: storyBoardID)
        
        return viewContoller!
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension MYPageViewController : UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if var index = arrayViewController.index(of: viewController){
            if index == 0 {
                return nil
            }
            
            index = index - 1
            
            return arrayViewController[index]
        }else{
            return nil
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if var index = arrayViewController.index(of: viewController){
            if index == arrayViewController.count - 1 {
                return nil
            }
            
            index = index + 1
            
            return arrayViewController[index]
        }else{
            return nil
        }
        
    }
    
    
}
